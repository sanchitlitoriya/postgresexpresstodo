const express = require('express');
const path = require('path');
const { router } = require('./routes/routes');
const { connect, disconnect } = require('./script/connection');

const app = express();
const port = process.env.PORT || 3000;

connect();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(path.resolve(), './script')));
app.use(express.static(path.join(path.resolve(), './style')));
app.use('/users', router);

app.get('/', (req, res) => {
  res.sendFile(path.join(path.resolve(), 'view/index.view.html'));
});

app.get('/toDo', (req, res) => {
  res.sendFile(path.join(path.resolve(), 'view/toDo.view.html'));
});

app.listen(port, () => {
  console.log(`server listning at http://localhost:${port}`);
});
