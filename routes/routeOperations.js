const { User } = require('../model/users');

/**
 * get all users info
 * @param {object} req request object
 * @param {object} res response object
 */
async function getAllUser(req, res) {
  const users = await User.findAll();
  res.send(users);
}

/**
 * get specific user info using constraints email and password
 * request.body must have email and password
 * @param {object} req request object
 * @param {object} res response object
 */
async function getUser(req, res) {
  const { email, password } = req.body;
  const users = await User.findAll({
    where: {
      email,
      password,
    },
    attributes: ['fname', 'lname', 'email', 'todo'],
  });

  const userFound = users.length;
  if (userFound) {
    res.send(users);
  } else {
    res.send([{ msg: 'invalid credentials' }]);
  }
}

/**
 * create a new user
 * request.body must have fanme email password lname is optional
 * @param {object} req request object
 * @param {object} res response object
 */
async function createUser(req, res) {
  try {
    const user = await User.create(
      {
        ...req.body,
        todo: [
        ],
      },
      {
        fields: ['fname', 'lname', 'email', 'password', 'todo'],
      },
    );

    res.send({ msg: 'user created successfully' });
  } catch (error) {
    res.send({ msg: error });
  }
}

/**
 * update todo for user with req.body.email
 * must have email and stringified todo array in request body
 * @param {object} req request object
 * @param {object} res responseobject
 */
async function updateTodo(req, res) {
  const { email, toDo } = req.body;
  const todo = JSON.parse(toDo);

  await User.update({ todo }, {
    where: {
      email,
    },
  });

  res.send({ msg: 'updated' });
}

/**
 * delete user with given email
 * request body must have email
 * @param {object} req request object
 * @param {object} res responseobject
 */
async function deleteUser(req, res) {
  const currentEmail = 'fname@mail.com';
  await User.destroy({
    where: {
      email: currentEmail,
    },
  });
  res.send({ msg: 'user Deleted' });
}

module.exports = {
  getAllUser,
  deleteUser,
  updateTodo,
  getUser,
  createUser,
};
