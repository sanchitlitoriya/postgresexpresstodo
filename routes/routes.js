const express = require('express');
// const { connect, disconnect } = require('./script/connection');

const {
  getAllUser, getUser, createUser, deleteUser, updateTodo,
} = require('./routeOperations.js');

const router = express.Router();

router.post('/all', getAllUser);
router.post('/user', getUser);
router.post('/create', createUser);
router.put('/update', updateTodo);
router.delete('/delete', deleteUser);

module.exports.router = router;
