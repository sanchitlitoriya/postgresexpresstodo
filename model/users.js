const { Sequelize, DataTypes } = require('sequelize');

const sequelize = new Sequelize('testdb', 'postgres', 'postgres', {
  host: 'localhost',
  dialect: 'postgres',
  logging: false,
});

const User = sequelize.define(
  'users',
  {
    // Model attributes are defined here
    fname: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lname: {
      type: DataTypes.STRING,
      // allowNull defaults to true
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    todo: {
      type: DataTypes.ARRAY({ type: DataTypes.STRING }),
    },
  },
  {
    createdAt: false,
    updatedAt: false,
  },
);

module.exports = {
  User,
  sequelize,
};
