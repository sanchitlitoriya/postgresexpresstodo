/**
 * init function
 */
(function init() {
  const todo = sessionStorage.getItem('toDo');
  if (sessionStorage.getItem('fname') && sessionStorage.getItem('email') && todo) {
    setToDo();

    const toDolist = JSON.parse(sessionStorage.getItem('toDo'));
    const table = document.getElementById('taskDone');
    toDolist.forEach((ele) => {
      if (ele[1] === 'true') {
        document.getElementById('done').style.display = 'block';
        // console.log(ele);
        const row = table.insertRow(table.rows.length);
        const task = row.insertCell(0);
        // const time = row.insertCell(1);
        // const date = row.insertCell(2);
        task.innerHTML = ele[0];
      }
    });
    // console.log();
  } else logout();
}());

/**
 *
 * @returns array of todo items
 */
function getTodo() {
  const toDolist = JSON.parse(sessionStorage.getItem('toDo'));
  return toDolist;
}
/**
 * this function sets todo to display.
 */
function setToDo() {
  const toDolist = getTodo();
  if (toDolist) {
    toDolist.forEach((element) => {
      if (element[1] === 'false') { createLi(element[0]); }
    });
  }
}
/**
 * this function creates list item
 * @param {array} element ['todo item','true/false']
 */
function createLi(element) {
  document.getElementById('list').style.display = 'block';
  const li = document.createElement('li');
  li.innerHTML = element;

  const crossBtn = document.createElement('button');
  crossBtn.innerHTML = '&#10060';
  crossBtn.onclick = function () {
    if (document.getElementById('toDosList').childElementCount === 1) {
      document.getElementById('list').style.display = 'none';
    }
    const toDolist = JSON.parse(sessionStorage.getItem('toDo'));
    const newToDoList = toDolist.filter((ele) => ele[0] !== element);
    sessionStorage.setItem('toDo', JSON.stringify(newToDoList));
    syncToDo();
    li.parentNode.removeChild(this.parentNode);
    // document.getElementById('done').style.display = 'block';
  };
  li.appendChild(crossBtn);

  const checkBtn = document.createElement('button');
  checkBtn.innerHTML = '&#9989';
  checkBtn.onclick = function () {
    if (document.getElementById('toDosList').childElementCount === 1) {
      document.getElementById('list').style.display = 'none';
    }
    markAsDone(element);
    addToDone(element);
    li.parentNode.removeChild(this.parentNode);
    document.getElementById('done').style.display = 'block';
  };
  li.appendChild(checkBtn);

  const ul = document.getElementById('toDosList');
  ul.appendChild(li);
}

/**
 * function to add todo item
 * onclick event of todo page add botton
 */
function addToDo() {
  const value = document.getElementById('toDoInput').value.trim();
  document.getElementById('toDoInput').value = '';
  if (value !== '') {
    const valueToPush = [value, 'false'];
    const toDolist = getTodo();
    toDolist.push(valueToPush);
    sessionStorage.setItem('toDo', JSON.stringify(toDolist));
    syncToDo();
    createLi(valueToPush[0]);
  } else {
    alert('No Todo Item found to add');
  }
}

/**
 * will set param[1] to true
 * @param {array} value - todo ['todoitem','true/false']
 */
function markAsDone(value) {
  const toDolist = JSON.parse(sessionStorage.getItem('toDo'));
  toDolist.forEach((ele) => {
    if (ele[0] === value) {
      ele[1] = 'true';
    }
  });
  sessionStorage.setItem('toDo', JSON.stringify(toDolist));
  syncToDo();
}

/**
 * add to task compleated table
 * @param {array} value - todo ['todoitem','true/false']
 */
function addToDone(value) {
  const toDolist = JSON.parse(sessionStorage.getItem('toDo'));
  const table = document.getElementById('taskDone');
  toDolist.forEach((ele) => {
    if (ele[0] === value && ele[1] === 'true') {
      const row = table.insertRow(table.rows.length);
      const task = row.insertCell(0);
      task.innerHTML = value;
    }
  });
}

/**
 * will set user info in marque text in todo page
 */
(function setHeaderData() {
  document.getElementById('headerName').innerHTML = `Hello ${sessionStorage.getItem('fname')} ${sessionStorage.getItem('lname')},    ${sessionStorage.getItem('email')}`;
}());

/**
 * handel logout
 * onclick logout button
 */
function logout() {
  // syncToDo();
  sessionStorage.clear();
  location.replace('/');
}

/**
 * fetch using put method
 * @param {string} url - url of put method route
 * @param {json} data - request.body
 * @returns response
 */
async function putData(url = '', data = {}) {
  const response = await fetch(url, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: JSON.stringify(data),
  });
  return response.json();
}

/**
 * update todo in database
 * uses put method
 */
function syncToDo() {
  const toDo = (sessionStorage.getItem('toDo'));
  const email = sessionStorage.getItem('email');
  putData('http://localhost:3000/users/update', { email, toDo });
}

/**
 * handle clearing of compeated task
 * onclick clear compleated button of todo page
 */
function clearCompleated() {
  document.getElementById('done').style.display = 'none';
  const toDo = JSON.parse(sessionStorage.getItem('toDo'));
  const newTodo = toDo.filter((ele) => ele[1] === 'false');
  sessionStorage.setItem('toDo', JSON.stringify(newTodo));
  syncToDo();
}
