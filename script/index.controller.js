/**
 * default init function
 */
(function init() {
  sessionStorage.clear();
}());
/**
 * function to fetch using post method
 * @param {string} url url of post route
 * @param {json} data request.body
 * @returns response.json / error
 */
async function postData(url = '', data = {}) {
  try {
    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: JSON.stringify(data),
    });
    return response.json();
  } catch (error) {
    return error;
  }
}

/**
 * set visible default register div and hide login div
 */
function openRegister() {
  document.getElementById('email').value = '';
  document.getElementById('lname').value = '';
  document.getElementById('fname').value = '';
  document.getElementById('password').value = '';
  document.getElementById('registerForm').style.display = 'block';
  document.getElementById('loginForm').style.display = 'none';
}

/**
 * set visible login div hide register div
 */
function openLogin() {
  document.getElementById('registerForm').style.display = 'none';
  document.getElementById('loginForm').style.display = 'block';
}

/**
 * handle login user
 * fetch data using input email and password
 * uses post method
 */
function login() {
  const validLogin = loginValidation();
  if (validLogin) {
    const emailLogin = document.getElementById('emailLogin').value;
    const passwordLogin = document.getElementById('passwordLogin').value;
    postData('http://localhost:3000/users/user', {
      email: emailLogin,
      password: passwordLogin,
    })
      .then((data) => {
        const {
          email, fname, lname, todo, msg,
        } = data[0];
        if (msg === 'invalid credentials') {
          document.getElementById('errorLogin').innerHTML = msg;
        } else {
          sessionStorage.setItem('email', email);
          sessionStorage.setItem('fname', fname);
          sessionStorage.setItem('lname', lname);
          sessionStorage.setItem('toDo', JSON.stringify(todo));
          location.replace('/toDo');
        }
        // console.log(data);
        //   setHeaderData();
        // send this data to todo app
      })
      .catch((err) => {
        document.getElementById('errorLogin').innerHTML = 'Internal error. please try again';
        console.log(err);
      }); // document.getElementById('errorLogin').innerHTML = 'Internal error. please try again');
  }
}

/**
 * handel register user
 */
function register() {
  const validRegister = registerValidation();
  if (validRegister) {
    const email = document.getElementById('email').value;
    const lname = document.getElementById('lname').value;
    const fname = document.getElementById('fname').value;
    const password = document.getElementById('password').value;
    postData('http://localhost:3000/users/create', {
      fname,
      lname,
      email,
      password,
    })
      .then((data) => {
        // console.log(data);
        if (data.msg === 'user created successfully') {
          alert(data.msg);
          openRegister();
        } else if (data.msg.errors[0].message === 'email must be unique') {
          document.getElementById('errorRegister').innerHTML = 'Email already registered';
          document.getElementById('email').value = '';
        }
        // send this data to todo app
      })
      .catch((err) => {
        // console.log(err);
        document.getElementById('errorLogin').innerHTML = 'Internal error. please try again';
      });
  }
}

/**
 * handle login validations
 * @returns boolean
 */
function loginValidation() {
  const email = document.getElementById('emailLogin').value.trim();
  const password = document.getElementById('passwordLogin').value.trim();
  if (email === '' || password === '') {
    document.getElementById('errorLogin').innerHTML = 'email & password cannot be empty';
    return false;
  }
  if (!email.match(/^\w+[\w-+\.]*\@\w+([-\.]\w+)*\.[a-zA-Z]{2,}$/)) {
    document.getElementById('errorLogin').innerHTML = 'Please enter a valid email';
    return false;
  }
  return true;
}

/**
 * handle register validations
 * @returns boolean
 */
function registerValidation() {
  const fname = document.getElementById('fname').value.trim();
  const email = document.getElementById('email').value.trim();
  const password = document.getElementById('password').value.trim();
  if (email === '' || password === '' || fname === '') {
    document.getElementById('errorRegister').innerHTML = 'fname, email & password cannot be empty';
    return false;
  }
  if (!email.match(/^\w+[\w-+\.]*\@\w+([-\.]\w+)*\.[a-zA-Z]{2,}$/)) {
    document.getElementById('errorRegister').innerHTML = 'Please enter a valid email';
    return false;
  }
  if (!fname.match(/^[A-Za-z]+$/)) {
    document.getElementById('errorRegister').innerHTML = 'Please enter a valid First name';
    return false;
  }
  return true;
}
