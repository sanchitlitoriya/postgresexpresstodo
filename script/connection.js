const { sequelize } = require('../model/users');

async function connect() {
  try {
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
}
function disconnect() {
  console.log('disconnected');
  sequelize.close();
}

module.exports = {
  connect,
  disconnect,
};
